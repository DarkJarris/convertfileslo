#!/bin/bash

INPUTFILE=$1
OUTPUTFILE=$2

display_usage() { 
	echo "Script converts one file type to another using LibreOffice."
	echo "Moves old format type into its own folder after the conversion"
	echo "LibreOffice must be closed for this script to work"
	echo "otherwise it will silently fail." 
	echo -e "\nUsage:convert.sh InputExtension OutputExtension \n"
	echo "Example: sh convert.sh docx pdf" 
	} 
	
# if less than two arguments supplied, display usage 
	if [  $# -le 1 ] 
	then 
		display_usage
		exit 1
	fi 
 
# check whether user had supplied -h or --help . If yes display usage 
	if [[ ( $# == "--help") ||  $# == "-h" ]] 
	then 
		display_usage
		exit 0
	fi 

# Call LibreOffice headless converter
soffice --convert-to $OUTPUTFILE *.$INPUTFILE  --headless

# Create folder in current directory for the old files
mkdir -p $INPUTFILE

# Move old files to newly created directory
mv *.$INPUTFILE $INPUTFILE