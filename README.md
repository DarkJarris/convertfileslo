### Overview ###

* Converts file formats using LibreOffice
* Version 1.0

Converts files from one format to another, then creates a folder to store all the old files to not clutter the current directory, and to allow you to mass clean at a later date if you wish.  

### Usage ###

clone or download the repo, and mark the file as executable  
```
chmod +x convert.sh  
```
    
    

Then run using the arguments InputType and OutputType  
example: Convert all ODT documents to PDF  
```
    sh convert.sh odt pdf  
```
example: Convert all WPS documents to DOCX  
```
    sh convert.sh wps docx  
```